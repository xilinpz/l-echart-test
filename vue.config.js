// module.exports = {
//     transpileDependencies: ['@dcloudio/uni-ui'],
// };

//  兼容echarts非wx平台可能会遇到echarts文件缺少wx判断导致无法使用
import { defineConfig } from 'vite';
import uni from '@dcloudio/vite-plugin-uni';

const UNI_PLATFORM = {
    app: 'uni',
    web: 'uni',
    'mp-weixin': 'wx',
    'mp-baidu': 'swan',
    'mp-alipay': 'my',
    'mp-toutiao': 'tt',
    'mp-lark': 'tt',
    'mp-qq': 'qq',
    'mp-kuaishou': 'ks',
    'mp-jd': 'jd',
    'mp-360': 'qh',
    'quickapp-webview-union': 'qa',
    'quickapp-webview-huawei': 'qa',
    'quickapp-webview': 'qa',
};

export default defineConfig({
    plugins: [uni()],
    define: {
        global: UNI_PLATFORM[process.env.UNI_PLATFORM],
        wx: UNI_PLATFORM[process.env.UNI_PLATFORM],
        transpileDependencies: ['@dcloudio/uni-ui'],
    },
});
