const automator = require('miniprogram-automator');

automator.launch({
  cliPath: 'path/to/cli', // 工具 cli 位置，如果你没有更改过默认安装位置，可以忽略此项
  projectPath: './dist/dev/mp-weixin', // 项目文件地址
}).then(async miniProgram => {
  // 打开后的回掉看自己需求编写
});